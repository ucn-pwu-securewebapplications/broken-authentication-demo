# Broken Authentication Demo

A web application vulnerable to broken access control involves illustrating a simple scenario where access controls are not properly enforced. Below is a basic example using a hypothetical web application that manages user profiles. This application will have a flaw in how it checks whether the logged-in user has the right to access or edit a profile.

## Scenario: Profile Management Application

This web application allows users to view and edit their profiles after logging in. However, due to broken access control, it's possible to access and edit other users' profiles by manipulating the URL.

## Backend Code Example (Node.js Application)

To create the given example in a Node.js environment, we'll use Express, a popular web application framework for Node.js. 

Here's how you can structure the Express application to illustrate the broken access control vulnerability:

```javascript  
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

// Simulate a simple user authentication and session mechanism
let sessions = {}; // This would ideally be managed more securely

// Middleware to check if a user is logged in
function isLoggedIn(req, res, next) {
    const sessionId = req.cookies['session_id'];
    if (!sessionId || !sessions[sessionId]) {
        return res.status(403).send('Please log in to view or edit profiles.');
    }
    req.user = sessions[sessionId];
    next();
}

app.get('/login', (req, res) => {
    // Simplified login that creates a session
    const userId = req.query.user_id;
    const sessionId = "session_" + Math.random().toString(36).substring(2); // Generate a simple session ID
    sessions[sessionId] = { user_id: userId };
    res.cookie('session_id', sessionId, { httpOnly: true });
    res.send(`Logged in as user ${userId}, with session ${sessionId}`);
});

app.get('/view_profile', isLoggedIn, (req, res) => {
    const user_id = req.query.user_id;
    // Missing access control check to verify if the logged-in user matches the requested user_id
    res.send(`Profile data for user: ${user_id}`);
});

app.post('/edit_profile', isLoggedIn, (req, res) => {
    const user_id = req.body.user_id;
    const new_data = req.body.data;
    // Missing access control check to verify if the logged-in user matches the user_id intended for edit
    res.send(`Profile updated for user: ${user_id} with new data: ${new_data}`);
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
```
### Notes

- This example uses a very simplistic approach to session management and authentication for demonstration purposes. Real-world applications should use secure session management and authentication mechanisms, such as OAuth2, JWT, or sessions managed by secure stores like Redis, and HTTPS for secure transmission.
- The `isLoggedIn` middleware simulates user authentication by checking for a session ID in the cookies, which is not secure and is just for illustrative purposes.
- The application is vulnerable to broken access control because it does not verify that the `user_id` in the request matches the `user_id` associated with the session (i.e., the logged-in user).

## Running the Example

1. Save the code in a file (e.g., `app.js`).
2. Run `npm install express body-parser cookie-parser` to install the required packages.
3. Start the application with `node app.js`.
4. Use a tool like Postman or cURL to interact with the application, observing how you can log in as one user but access or edit another user's profile by manipulating the `user_id` in the URLs or body of the requests.


## Vulnerability Explanation

In this simple application, both `/view_profile` and `/edit_profile` endpoints are vulnerable to broken access control. Although there's a check to ensure the user is logged in, there's no verification to ensure the `user_id` requested or submitted for profile viewing or editing matches the `user_id` of the currently logged-in user. An attacker could simply change the `user_id` parameter in the URL or in the data submitted to the `/edit_profile` endpoint to view or edit another user's profile without authorization.

### Mitigation Strategies

- **User Verification:** Implement checks to ensure that the `user_id` making the request matches the `user_id` associated with the requested resource. Only allow users to access or modify their own data unless they have specific roles (e.g., administrators).
- **Use Session Management:** Instead of relying on user input to determine which profile to view or edit, use the user's session information to retrieve the appropriate user ID.
- **Role-Based Access Control (RBAC):** Ensure that only users with the appropriate roles or permissions can access sensitive functionality or data.


