const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());

// Simulate a simple user authentication and session mechanism
let sessions = {}; // This would ideally be managed more securely

// Middleware to check if a user is logged in
function isLoggedIn(req, res, next){
    const sessionId = req.cookies['session_id'];
    if(!sessionId || !sessions[sessionId]){
        return res.status(403).send('Please log in to view or edit profiles');
    }
    req.user = sessions[sessionId];
    next();
}

app.post('/login', (req, res) => {
    // Simplified login that creates a session
    const userId = req.query.user_id;
    const sessionId = "session_" + Math.random().toString(36).substring(2); // Generate a simple session ID
    sessions[sessionId] = {user_id: userId};
    res.cookie('session_id', sessionId, {httpOnly:true});
    res.send(`Logged in as user ${userId}, with session ${sessionId}`)
});

app.get('/view_profile', isLoggedIn, (req, res) => {
    const user_id = req.query.user_id;
    // Missing access control check to verify if the logged-in user matches the requested user_id
    res.send(`Profile data for user: ${user_id}`);
});

app.put('/edit_profile', isLoggedIn, (req, res) => {
    const user_id = req.body.user_id;
    const new_data = req.body.data;
    // Missing access control check to verify if the logged-in user matches the user_id intended for edit
    res.send(`Profile updated for user: ${user_id} with new data: ${new_data}`);
});

app.listen(port, ()=>{
    console.log(`Example app listening at http://localhost:${port}`);
})

